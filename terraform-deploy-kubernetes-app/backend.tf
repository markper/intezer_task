terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/28860704/terraform/state/terraform"
    lock_address   = "https://gitlab.com/api/v4/projects/28860704/terraform/state/terraform/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/28860704/terraform/state/terraform/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"

  }
}
